import pymysql
from google.cloud.sql.connector import Connector
import sqlalchemy
import json
from datetime import datetime


class MySQLConnector():

    def __init__(self, configOptions):
        self.connector = Connector()
        self.configOptions = configOptions

    def getConn(self):
        conn = self.connector.connect(
            self.configOptions['connectionString'],
            "pymysql",
            user=self.configOptions['user'],
            password=self.configOptions['password'],
            db=self.configOptions['db']
        )
        return conn

    def connection(self):

        # create connection pool with 'creator' argument to our connection object function
        self.pool = sqlalchemy.create_engine(
            "mysql+pymysql://",
            creator=self.getConn,
        )

    def close(self):
        self.connector.close()

    def reconnect(self):
        self.close()
        self.connection()

    def returnDoc(selfs, entry):
        return json.loads(entry[0])

    def getDocsByFields(self, sqlString):
        with self.pool.connect() as db_conn:
            results = db_conn.execute(sqlalchemy.text(sqlString)).fetchall()
            print(results)
        # connector.close()
        return_list = []
        for result in results:
            return_list.append(self.returnDoc(result))
        return return_list

    def getDocByFields(self, sqlString):
        with self.pool.connect() as db_conn:
            results = db_conn.execute(sqlalchemy.text(sqlString)).fetchone()

            print(results)

        return results[0][0]

    def getDocsById(self, id, collectionName):
        with self.pool.connect() as db_conn:
            results = db_conn.execute(sqlalchemy.text(
                f"SELECT * FROM {collectionName} WHERE JSON_EXTRACT(doc ,'$._id') =  '{id}';")).fetchall()

            print(results)
            print(results[0]['_id'])
        return_list = []
        for result in results:
            return_list.append(self.returnDoc(result))
        return return_list

    def getDocById(self, id, collectionName):
        with self.pool.connect() as db_conn:
            results = db_conn.execute(sqlalchemy.text(
                f"SELECT * FROM {collectionName} WHERE JSON_EXTRACT(doc ,'$._id') =  '{id}';")).fetchall()

            print(results)
            print(results[0]['_id'])
        return results[0][0]

    def getRows(self, sqlString):
        with self.pool.connect() as db_conn:
            results = db_conn.execute(sqlalchemy.text(sqlString)).fetchall()

            print(results)

        return_list = []
        for result in results:
            return_list.append(self.returnDoc(result))
        return return_list

    def getRow(self, sqlString):
        with self.pool.connect() as db_conn:
            results = db_conn.execute(sqlalchemy.text(sqlString)).fetchone()

            print(results)
        return results[0][0]

    def insertDocs(self, collectionName, docs):
        with self.pool.connect() as db_conn:
            for doc in docs:
                now = int(datetime.now().timestamp())
                result = db_conn.execute(sqlalchemy.text(
                    f"""insert into {collectionName} (doc) values ('{json.dumps(doc.update({'createdAt': now, 'lastUpdated': now}))}');"""
                ))
                print(result)

    def insertDocs(self, collectionName, docs):
        with self.pool.connect() as db_conn:
            last = db_conn.execute(sqlalchemy.text(
                "select * from tips order by _id DESC limit 1;"))
            print(last)
            last_id = json.loads(last[0]['_id'])
            new_id = str.encode(
                f'{last_id[:4]}{hex(int(datetime.now().timestamp()))}{0:#0{16-len(hex(int(last_id[12:], 16) + 1))}}{hex(int(last_id[12:], 16) + 1)}'.replace("x", ""))

            print(new_id)
            for doc in docs:
                now = int(datetime.now().timestamp())
                result = db_conn.execute(sqlalchemy.text(f"""insert into {collectionName} (doc) values ('${json.dumps(doc.update({'id': new_id,
                  'createdAt': now,
                  'lastUpdatedAt': now}))}');"""))
                print(result)

    def insertRows(self, headers, tableName, rowsArray):
        with self.pool.connect() as db_conn:
            for row in rowsArray:
                rowString = ""
                for column in row:
                    if isinstance(column, str):
                        rowString = f"{rowString} '{column}',"
                    elif isinstance(column, bool):
                        rowString = f"{rowString} {column},"
                    else:
                        rowString = f"{rowString} ${column},"
                now = int(datetime.now().timestamp())
                result = db_conn.execute(sqlalchemy.text(
                    f"insert into {tableName} ({', '.join(headers)} , lastUpdated) values ({rowString} {int(now.timestamp())});"))
                print(result)

    def updateRows(self, tableName, updateObjs, whereString):
        with self.pool.connect() as db_conn:
            for row in updateObjs:
                setString = ""
                for key, value in row.items():

                    if isinstance(value, str):
                        setString = f"{setString} {key} = '{value}',"
                    elif isinstance(value, bool):
                        setString = f"{setString} {key} = {value},"
                    else:
                        setString = f"{setString} {key} = {value},"

                now = int(datetime.now().timestamp())
                result = db_conn.execute(sqlalchemy.text(
                    f"UPDATE {tableName} SET {setString} lastUpdated = {now} where {whereString}"))
                print(result)

    def updateDocs(self, collectionName, updateObjs, whereString):
        with self.pool.connect() as db_conn:
            for row in updateObjs:
                result = db_conn.execute(sqlalchemy.text(
                    f"select * from {collectionName} where {whereString}")).fetchone()

                print(result)
                oldDoc = json.loads(result[0][0])
                newDoc = oldDoc.update(row)
                newDoc = newDoc.update(
                    {"lastUpdated": int(datetime.now().timestamp())})

                updated = result = db_conn.execute(sqlalchemy.text(
                    f"UPDATE {collectionName} SET doc = '{json.loads(newDoc)}' where {whereString}"))

                print(updated)
